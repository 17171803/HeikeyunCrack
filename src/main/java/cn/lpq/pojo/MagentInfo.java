package cn.lpq.pojo;

public class MagentInfo {

	private String fileName;

	private String fileSize;

	private String magent;

	public MagentInfo(String fileName, String fileSize, String magent) {
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.magent = magent;
	}

	public MagentInfo() {
		
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getMagent() {
		return magent;
	}

	public void setMagent(String magent) {
		this.magent = magent;
	}

}
